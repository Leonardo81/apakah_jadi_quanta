from django.shortcuts import render,redirect
from .import forms
from .models import Message,Comment

def message_list(request):
    messages = Message.objects.all()
    message_form = forms.CreateMessageForm()
    comment_form = forms.CommentForm()
    response = {
         'msgs' : messages,
         'form' : message_form,
         'comment_form' : comment_form
    }
    return render(request, 'msg/message_list.html', response)

def message_create(request):
    message_form = forms.CreateMessageForm(request.POST)
    if request.method == "POST" and message_form.is_valid():
        Message.objects.create(
            submiter = message_form.cleaned_data['submiter'],
            message = message_form.cleaned_data['message']
        )
    return redirect('msg:message_list')

def comment_create(request, message_id):
    comment_form = forms.CommentForm(request.POST)
    if request.method == "POST" and comment_form.is_valid():
        Comment.objects.create(
            submiter = comment_form.cleaned_data['submiter'],
            comment = comment_form.cleaned_data['comment'],
            message = Message.objects.get(id=message_id)
        )
    return redirect('msg:message_list')