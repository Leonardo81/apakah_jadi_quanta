from django.db import models

class Message(models.Model):
    submiter = models.CharField(max_length=50)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return "{} - {}".format(self.submiter, self.snipped_message())
    
    def snipped_message(self):
        if len(self.message) > 20 :
            return self.message[:20] + " ..."
        return self.message

class Comment(models.Model):
    submiter = models.CharField(max_length=50)
    comment = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    message = models.ForeignKey(Message, on_delete=models.CASCADE, blank=True, null=True, related_name="comments")

    def __str__(self):
        return "{} - {}".format(self.submiter, self.comment)