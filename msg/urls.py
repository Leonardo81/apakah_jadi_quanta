from . import views
from django.urls import path
app_name = 'msg'

urlpatterns = [
    path('', views.message_list, name='message_list'),
    path('message_create/', views.message_create, name="message_create"),
    path('comment_create/<int:message_id>/', views.comment_create, name="comment_create"),           
]
